import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {UsersResolver} from './resolvers/users.resolver';
import {PhotosComponent} from './photos/photos.component';
import {AlbumsComponent} from './albums/albums.component';
import {AlbumsResolver} from './resolvers/albums.resolver';
import {PhotosResolver} from './resolvers/photos.resolver';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    resolve: {
      users: UsersResolver
    },
  },
  {
    path: 'user/:user',
    component: AlbumsComponent,
    resolve: {
      albums: AlbumsResolver
    }
  },
  {
    path: 'album/:album',
    component: PhotosComponent,
    resolve: {
      photos: PhotosResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
