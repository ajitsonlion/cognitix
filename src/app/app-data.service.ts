import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_BASE} from './app.consts';
import {Album, Photo, User} from './app.model';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AppDataService {

  constructor(private _http: HttpClient) {
  }

  getUsers(): Observable<User[]> {

    return this._http.get<User[]>(`${API_BASE}/users`);

  }


  getAlbumsForUser(id: string): Observable<Album[]> {

    return this._http.get<Album[]>(`${API_BASE}/albums/?userId=${id}`);

  }

  getPhotosFromAlbum(id: number): Observable<Photo[]> {
    return this._http.get<Photo[]>(`${API_BASE}/photos/?albumId=${id}`);

  }
}
