import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Photo} from '../app.model';

@Component({
  selector: 'app-photos',
  template: `
    <app-photo *ngFor="let photo of photos" [photo]="photo"></app-photo>
  `,

  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PhotosComponent implements OnInit {

  photos: Photo[] = [];

  constructor(private _route: ActivatedRoute) {
  }

  ngOnInit() {
    this.photos = this._route.snapshot.data['photos'];

  }


}
