import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Photo} from '../app.model';

@Component({
  selector: 'app-photo',
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title><h2>{{ photo.title }}</h2></mat-card-title>
      </mat-card-header>
      <mat-card-content>
        <img [src]="photo.thumbnailUrl">
      </mat-card-content>
      <mat-card-actions align="start">
        <a [href]="photo.url">
          <button mat-raised-button color="primary">View</button>
        </a>
      </mat-card-actions>
    </mat-card>
  `,
  styles: [`
    img {
      height: 250px;
      width: 250px;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PhotoComponent {


  @Input() photo: Photo;


}
