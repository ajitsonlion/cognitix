import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PhotosComponent} from './photos.component';
import {MatCardModule} from '@angular/material';
import {MockSandboxService} from '../mocks/mock.sandbox';
import {ActivatedRoute} from '@angular/router';
import {AppSandBox} from '../app.sandbox';
import {ActivatedRouteStub} from '../mocks/mock-activated-route';
import {PhotoComponent} from './photo.component';
import {By} from '@angular/platform-browser';

describe('PhotosComponent', () => {
  let component: PhotosComponent;
  let fixture: ComponentFixture<PhotosComponent>;
  let activatedRoute: ActivatedRouteStub;
  let sandbox: MockSandboxService;

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    sandbox = new MockSandboxService();
  });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PhotosComponent, PhotoComponent],
      imports: [MatCardModule],
      providers: [{provide: AppSandBox, useValue: sandbox}, {provide: ActivatedRoute, useValue: activatedRoute},
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotosComponent);
    activatedRoute.testResolvedData = {
      photos: [{}]
    };

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have same PhotoComponent components as number of photos', () => {
    const userCards = fixture.debugElement.queryAll(By.directive(PhotoComponent)).length;
    expect(userCards).toEqual(1);
  });
});
