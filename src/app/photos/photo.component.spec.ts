import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PhotoComponent} from './photo.component';
import {MatButtonModule, MatCardModule, MatRippleModule} from '@angular/material';
import {Photo} from '../app.model';
import {By} from '@angular/platform-browser';

const MOCK_PHOTO: Photo = {
  'albumId': 1,
  'id': 1,
  'title': 'accusamus beatae ad facilis cum similique qui sunt',
  'url': 'http://placehold.it/600/92c952',
  'thumbnailUrl': 'http://placehold.it/150/92c952'
};
describe('PhotoComponent', () => {
  let component: PhotoComponent;
  let fixture: ComponentFixture<PhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PhotoComponent],
      imports: [MatCardModule,
        MatRippleModule,
        MatButtonModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoComponent);
    component = fixture.componentInstance;
    component.photo = MOCK_PHOTO;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have same  mat-card-title inner text as MOCK_PHOTO title', () => {
    const cartTitle = fixture.debugElement.query(By.css('mat-card-title')).nativeElement;
    expect(MOCK_PHOTO.title).toEqual(cartTitle.innerText.trim());
  });

  it('should have button to view album photos', () => {
    const cartTitle = fixture.debugElement.query(By.css('button')).nativeElement;
    expect('View').toEqual(cartTitle.innerText);
  });

});
