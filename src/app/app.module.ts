import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppDataService} from './app-data.service';
import {AppSandBox} from './app.sandbox';
import {UsersComponent} from './users/users.component';
import {UserComponent} from './users/user.component';
import {UsersResolver} from './resolvers/users.resolver';
import {MatButtonModule, MatCardModule, MatRippleModule} from '@angular/material';
import {AlbumsComponent} from './albums/albums.component';
import {AlbumComponent} from './albums/album.component';
import {PhotosComponent} from './photos/photos.component';
import {PhotoComponent} from './photos/photo.component';
import {AlbumsResolver} from './resolvers/albums.resolver';
import {PhotosResolver} from './resolvers/photos.resolver';
import { UserAddressComponent } from './users/user-address.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    AlbumsComponent,
    AlbumComponent,
    PhotosComponent,
    PhotoComponent,
    UserAddressComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MatCardModule,
    MatRippleModule,
    MatButtonModule,
    AppRoutingModule
  ],
  providers: [AppDataService, AppSandBox, UsersResolver, AlbumsResolver, PhotosResolver],
  bootstrap: [AppComponent]
})
export class AppModule {
}
