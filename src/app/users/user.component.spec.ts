import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserComponent} from './user.component';
import {MatButtonModule, MatCardModule, MatRippleModule} from '@angular/material';
import {User} from '../app.model';
import {By} from '@angular/platform-browser';
import {UserAddressComponent} from './user-address.component';

const MOCK_USER: User = {
  'id': 1,
  'name': 'Leanne Graham',
  'username': 'Bret',
  'email': 'Sincere@april.biz',
  'address': {
    'street': 'Kulas Light',
    'suite': 'Apt. 556',
    'city': 'Gwenborough',
    'zipcode': '92998-3874',
    'geo': {
      'lat': '-37.3159',
      'lng': '81.1496'
    }
  },
  'phone': '1-770-736-8031 x56442',
  'website': 'hildegard.org',
  'company': {
    'name': 'Romaguera-Crona',
    'catchPhrase': 'Multi-layered client-server neural-net',
    'bs': 'harness real-time e-markets'
  }
};

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserComponent, UserAddressComponent],
      imports: [MatCardModule,
        MatRippleModule,
        MatButtonModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    component.user = MOCK_USER;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have same  mat-card-title inner text as MOCK_USER name', () => {
    const cartTitle = fixture.debugElement.query(By.css('mat-card-title')).nativeElement;
    expect(MOCK_USER.name).toEqual(cartTitle.innerText.trim());
  });

  it('should have button to view user albums', () => {
    const cartTitle = fixture.debugElement.query(By.css('button')).nativeElement;
    expect('Albums').toEqual(cartTitle.innerText);
  });
});
