import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from '../app.model';

@Component({
  selector: 'app-user',
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title><h2>{{ user.name }}</h2></mat-card-title>
      </mat-card-header>
      <mat-card-content>
        <app-user-address [address]="user.address"></app-user-address>
      </mat-card-content>
      <mat-card-actions align="start">
        <button mat-raised-button color="primary" (click)="userSelection(user)">Albums</button>
      </mat-card-actions>
    </mat-card>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserComponent {

  @Input() user: User;

  @Output() onUserSelection = new EventEmitter<User>();

  userSelection(user: User) {
    this.onUserSelection.emit(user);

  }

}
