import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AppSandBox} from '../app.sandbox';
import {User} from '../app.model';

@Component({
  selector: 'app-users',
  template: `
    <app-user *ngFor="let user of users" [user]="user" (onUserSelection)="viewUserAlbums($event)"></app-user>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class UsersComponent implements OnInit {

  users: User[] = [];

  constructor(private _route: ActivatedRoute,
              private _sandbox: AppSandBox) {
  }

  ngOnInit() {
    this.users = this._route.snapshot.data['users'];
  }

  viewUserAlbums(user: User) {

    this._sandbox.onUserSelect(user);
  }
}
