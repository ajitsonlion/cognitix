import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserAddressComponent} from './user-address.component';
import {By} from '@angular/platform-browser';
import {Address} from '../app.model';

const MOCK_ADDRESS: Address = {
  'street': 'Kulas Light',
  'suite': 'Apt. 556',
  'city': 'Gwenborough',
  'zipcode': '92998-3874',
  'geo': {
    'lat': '-37.3159',
    'lng': '81.1496'
  }
};
describe('UserAddressComponent', () => {
  let component: UserAddressComponent;
  let fixture: ComponentFixture<UserAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserAddressComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAddressComponent);
    component = fixture.componentInstance;
    component.address = MOCK_ADDRESS;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have button to view album photos', () => {
    const cartTitle = fixture.debugElement.query(By.css('address')).nativeElement;
    expect(cartTitle).toBeTruthy();
  });
});
