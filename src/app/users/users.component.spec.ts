import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UsersComponent} from './users.component';
import {MatCardModule} from '@angular/material';
import {MockSandboxService} from '../mocks/mock.sandbox';
import {ActivatedRouteStub} from '../mocks/mock-activated-route';
import {ActivatedRoute} from '@angular/router';
 import {By} from '@angular/platform-browser';
import {UserComponent} from './user.component';
import {AppSandBox} from '../app.sandbox';
import {UserAddressComponent} from './user-address.component';


describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let activatedRoute: ActivatedRouteStub;
  let sandbox: MockSandboxService;

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    sandbox = new MockSandboxService();
  });


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsersComponent, UserComponent, UserAddressComponent],
      imports: [MatCardModule],
      providers: [{provide: AppSandBox, useValue: sandbox}, {provide: ActivatedRoute, useValue: activatedRoute},
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);

    activatedRoute.testResolvedData = {
      users: []
    };
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have same UserComponent components as number of users', () => {
    const userCards = fixture.debugElement.queryAll(By.directive(UserComponent)).length;
    expect(userCards).toEqual(0);
  });

});
