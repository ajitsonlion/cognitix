import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Address} from '../app.model';

@Component({
  selector: 'app-user-address',
  template: `
    <address>
      {{ address.street }},
      {{ address.suite }},
      {{ address.zipcode }},
      {{ address.city }},
      Lat: {{ address.geo.lat }}, Long: {{ address.geo.lng }}
    </address>

  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAddressComponent {

  @Input() address: Address;


}
