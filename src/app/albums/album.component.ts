import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Album} from '../app.model';

@Component({
  selector: 'app-album',
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title><h2>{{ album.title }}</h2></mat-card-title>
      </mat-card-header>
      <mat-card-actions align="start">
        <button mat-raised-button color="primary" (click)="albumSelection(album)">Photos</button>
      </mat-card-actions>

    </mat-card>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumComponent {

  @Input() album: Album;

  @Output() onAlbumSelection = new EventEmitter<Album>();

  albumSelection(album: Album) {
    this.onAlbumSelection.emit(album);
  }

}
