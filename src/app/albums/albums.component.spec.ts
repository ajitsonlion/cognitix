import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AlbumsComponent} from './albums.component';
import {MatCardModule} from '@angular/material';
import {MockSandboxService} from '../mocks/mock.sandbox';
import {ActivatedRouteStub} from '../mocks/mock-activated-route';
import {ActivatedRoute} from '@angular/router';
import {AppSandBox} from '../app.sandbox';
import {AlbumComponent} from './album.component';
import {By} from '@angular/platform-browser';

describe('AlbumsComponent', () => {
  let component: AlbumsComponent;
  let fixture: ComponentFixture<AlbumsComponent>;
  let activatedRoute: ActivatedRouteStub;
  let sandbox: MockSandboxService;

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    sandbox = new MockSandboxService();
  });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlbumsComponent, AlbumComponent],
      imports: [MatCardModule],
      providers: [{provide: AppSandBox, useValue: sandbox}, {provide: ActivatedRoute, useValue: activatedRoute},
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumsComponent);
    activatedRoute.testResolvedData = {
      albums: [{}]
    };
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have same AlbumComponent components as number of albums', () => {
    const userCards = fixture.debugElement.queryAll(By.directive(AlbumComponent)).length;
    expect(userCards).toEqual(1);
  });
});
