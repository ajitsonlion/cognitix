import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AlbumComponent} from './album.component';
import {MatButtonModule, MatCardModule, MatRippleModule} from '@angular/material';
import {Album} from '../app.model';
import {By} from '@angular/platform-browser';

const MOCK_ALBUM: Album = {
  'userId': 1,
  'id': 1,
  'title': 'quidem molestiae enim'
};
describe('AlbumComponent', () => {
  let component: AlbumComponent;
  let fixture: ComponentFixture<AlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlbumComponent],
      imports: [MatCardModule,
        MatRippleModule,
        MatButtonModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumComponent);
    component = fixture.componentInstance;
    component.album = MOCK_ALBUM;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have same  mat-card-title inner text as MOCK_ALBUM title', () => {
    const cartTitle = fixture.debugElement.query(By.css('mat-card-title')).nativeElement;
    expect(MOCK_ALBUM.title).toEqual(cartTitle.innerText.trim());
  });

  it('should have button to view album photos', () => {
    const cartTitle = fixture.debugElement.query(By.css('button')).nativeElement;
    expect('Photos').toEqual(cartTitle.innerText);
  });
});
