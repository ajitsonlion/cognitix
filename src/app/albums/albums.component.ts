import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Album} from '../app.model';
import {AppSandBox} from '../app.sandbox';

@Component({
  selector: 'app-albums',
  template: `
    <app-album *ngFor="let album of albums" [album]="album" (onAlbumSelection)="openAlbum($event)"></app-album>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class AlbumsComponent implements OnInit {
  albums: Album[] = [];

  constructor(private _route: ActivatedRoute,
              private _sandbox: AppSandBox) {
  }

  ngOnInit() {
    this.albums = this._route.snapshot.data['albums'];

  }

  openAlbum(album: Album) {
    this._sandbox.onAlbumSelect(album);

  }
}
