import {SandBox} from './sand-box';
import {Injectable} from '@angular/core';
import {AppDataService} from './app-data.service';
import {Observable} from 'rxjs/Observable';
import {Album, Photo, User} from './app.model';
import {Router} from '@angular/router';

@Injectable()
export class AppSandBox implements SandBox {

  constructor(private _dataService: AppDataService,
              private _router: Router,) {
  }


  loadUsers(): Observable<User[]> {
    return this._dataService.getUsers();
  }

  loadAlbumsForUser(id: string): Observable<Album[]> {

    return this._dataService.getAlbumsForUser(id);

  }

  loadPhotosFromAlbum(albumId: number): Observable<Photo[]> {
    return this._dataService.getPhotosFromAlbum(albumId);
  }

  onUserSelect(user: User) {

    this._router.navigate(['', 'user', user.id]);
  }

  onAlbumSelect(album: Album) {
    this._router.navigate(['', 'album', album.id]);

  }
}



