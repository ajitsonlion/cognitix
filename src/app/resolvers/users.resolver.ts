import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {User} from '../app.model';
import {Observable} from 'rxjs/Observable';
import {AppSandBox} from '../app.sandbox';

@Injectable()
export class UsersResolver implements Resolve<User[]> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User[]> | Promise<User[]> | User[] {
    return this._sandbox.loadUsers();
  }

  constructor(private _sandbox: AppSandBox) {
  }

}
