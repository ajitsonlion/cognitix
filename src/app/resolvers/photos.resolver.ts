import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
 import {AppSandBox} from '../app.sandbox';
import {Photo} from '../app.model';

@Injectable()
export class PhotosResolver implements Resolve<Photo[]> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Photo[] | Observable<Photo[]> | Promise<Photo[]> {
    return this._sandbox.loadPhotosFromAlbum(route.params.album);
  }

  constructor(private _sandbox: AppSandBox) {
  }

}
