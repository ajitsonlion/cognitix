import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AppSandBox} from '../app.sandbox';
import {Album} from '../app.model';

@Injectable()
export class AlbumsResolver implements Resolve<Album[]> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Album[] | Observable<Album[]> | Promise<Album[]> {
    return this._sandbox.loadAlbumsForUser(route.params.user);
  }

  constructor(private _sandbox: AppSandBox) {
  }

}
