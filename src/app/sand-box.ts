import {User} from './app.model';
import {Observable} from 'rxjs/Observable';

export interface SandBox {

  loadUsers(): Observable<User[]> | User[];


}
