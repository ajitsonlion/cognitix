import {SandBox} from '../sand-box';
import {Observable} from 'rxjs/Observable';
import {User} from '../app.model';

export class MockSandboxService implements SandBox {
  loadUsers(): Observable<User[]> | User[] {
    return undefined;
  }


}
