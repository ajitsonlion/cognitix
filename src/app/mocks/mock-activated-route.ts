import {Injectable} from '@angular/core';

@Injectable()
export class ActivatedRouteStub {

  private params: {};

  set testResolvedData(params: any) {
    this.params = params;
  }

  // ActivatedRoute.snapshot.data['users']
  get snapshot() {
    return {
      data: this.params
    };
  }
}
