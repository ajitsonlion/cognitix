# Cognitix

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Development server

Run `npm start` for a dev server.
Run `npm run hmr` for a dev server with hot reload. 
This needs `npm install` to be done first.
Navigate to `http://localhost:4200/`. The app will automatically reload or hot reload if you change any of the source files.

## Production Build
Run `npm run build` to build the project. 
This needs `npm install` to be done first.
The build artifacts will be stored in the `dist/` directory.

## Host the prod build
Run `npm run host` to build the production mode then host it. 
This needs `npm install` and `npm run build` to be done first.
It will start basic http server  at [http://127.0.0.1:8080](http://127.0.0.1:8080) that will serve from `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
Alternatively, wallaby.js is configured, and could be used for test as you code.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
