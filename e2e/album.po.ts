import {browser, by, element} from 'protractor';

export class UserAlbumPage {
  navigateTo() {
    return browser.get('/user/1');
  }

  getAlbumTag() {
    return element(by.tagName('app-albums'));
  }

  getAlbumsList() {
    return element(by.tagName('app-album'));
  }


}
