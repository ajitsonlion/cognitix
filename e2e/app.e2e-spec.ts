import {AppPage} from './app.po';

describe('cognitix App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('app-root tag exists', () => {
    page.navigateTo();
    expect(page.getRootNode()).toBeTruthy();
  });
  it('router-outlet tag exists', () => {
    page.navigateTo();
    expect(page.getRouterOutlet()).toBeTruthy();
  });

  it('app-users should get loaded as default route component', () => {
    page.navigateTo();
    expect(page.getUsersList()).toBeTruthy();
  });
});
