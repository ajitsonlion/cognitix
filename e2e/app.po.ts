import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getRootNode() {
    return element(by.tagName('app-root'));
  }

  getRouterOutlet() {
    return element(by.tagName('router-outlet'));
  }

  getUsersList() {
    return element(by.tagName('app-users'));
  }
}
