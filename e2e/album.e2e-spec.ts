import {UserAlbumPage} from './album.po';

describe('cognitix App', () => {
  let page: UserAlbumPage;

  beforeEach(() => {
    page = new UserAlbumPage();
  });

  it('app-albums should be present', () => {
    page.navigateTo();
    expect(page.getAlbumTag()).toBeTruthy();
  });

  it('should have list of album', () => {
    page.navigateTo();
    expect(page.getAlbumsList()).toBeTruthy();
  });

});
